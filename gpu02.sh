#!/bin/sh
# Date: 2023/08/022
#
# Enable IO-MMU on PVE Server

echo ""
echo "********************************************"
echo "***  Enable IO-MMU on proxmox host       ***"
echo "********************************************"
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet.*"/GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on iommu=pt"/g' /etc/default/grub

echo ""
echo "    Update grub .... "
update-grub

echo "vfio"  > /etc/modules
echo "vfio_iommu_type1"  >> /etc/modules
echo "vfio_pci"  >> /etc/modules
echo "vfio_virqfd"  >> /etc/modules

echo "options vfio_iommu_type1 allow_unsafe_interrupts=1" > /etc/modprobe.d/iommu_unsafe_interrupts.conf
echo "options kvm ignore_msrs=1 report_ignored_msrs=0" > /etc/modprobe.d/kvm.conf
echo "blacklist nouveau" > /etc/modprobe.d/disable-nouveau.conf
echo "options nouveau modeset=0" >> /etc/modprobe.d/disable-nouveau.conf

echo ""
echo "    Proxmox set vgpu_unlock .... "
cd ~
#git clone https://github.com/sirpdboy/vgpu-proxmox.git vgpu-proxmox
#chmod +x ~/vgpu-proxmox/*.sh

cd /opt
git clone https://gitlab.com/sirpdboy/vgpu_unlock-rs.git
# git clone https://github.com/sirpdboy/vgpu_unlock-rs.git
curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal
source $HOME/.cargo/env
cd vgpu_unlock-rs/
cargo build --release
cd ~

mkdir /etc/vgpu_unlock
cp -r ~/vgpu-proxmox/profile_override.toml /etc/vgpu_unlock/profile_override.toml
mkdir /etc/systemd/system/{nvidia-vgpud.service.d,nvidia-vgpu-mgr.service.d}
echo -e "[Service]\nEnvironment=LD_PRELOAD=/opt/vgpu_unlock-rs/target/release/libvgpu_unlock_rs.so" > /etc/systemd/system/nvidia-vgpud.service.d/vgpu_unlock.conf
echo -e "[Service]\nEnvironment=LD_PRELOAD=/opt/vgpu_unlock-rs/target/release/libvgpu_unlock_rs.so" > /etc/systemd/system/nvidia-vgpu-mgr.service.d/vgpu_unlock.conf

update-initramfs -u -k all
